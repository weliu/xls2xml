"use strict"

var app = new Vue({
  el: "#app",
  data() {
    return {
      form: {
        manufactureDate: '',
        transactionID: '',
        supplierNo: '',
        supplierName: '',
        brandName: '',
        itemNo: '',
        itemStyle: '',
        batchNo: ''
      },
      loading: false,
      rules: {
        manufactureDate: [
          { required: true, message: '请输入 物料制造日期', trigger: 'blur' }
        ],
        transactionID: [
          { required: true, message: '请输入 记录编号', trigger: 'blur' }
        ],
        supplierNo: [
          { required: true, message: '请输入 供方编号', trigger: 'blur' }
        ],
        supplierName: [
          { required: true, message: '请输入 供方名称', trigger: 'blur' }
        ],
        brandName: [
          { required: true, message: '请输入 供方品牌', trigger: 'blur' }
        ],
        itemNo: [
          { required: true, message: '请输入 物料代码', trigger: 'blur' }
        ],
        itemStyle: [
          { required: true, message: '请输入 规格型号', trigger: 'blur' }
        ],
        batchNo: [
          { required: true, message: '请输入 批次号', trigger: 'blur' }
        ]
      }
    }
  },
  methods: {
    transform(params) {
      this.$refs['form'].validate((valid) => {
        if (!valid) { return false }
        const formData = new FormData()
        formData.append('file', params.file)
        formData.append('ManufactureDate', this.form.manufactureDate)
        formData.append('TransactionID', this.form.transactionID)
        formData.append('SupplierNo', this.form.supplierNo)
        formData.append('SupplierName', this.form.supplierName)
        formData.append('BrandName', this.form.brandName)
        formData.append('ItemNo', this.form.itemNo)
        formData.append('ItemStyle', this.form.itemStyle)
        formData.append('BatchNo', this.form.batchNo)
        this.loading = true
        axios.post('/xls2xml', formData, { headers: { 'Content-Type': 'multipart/form-data' } }).then(response => {
          this.loading = false
          const blob = new Blob([response.data])
          const now = new Date()
          const fileName = 'report_' + now.getFullYear() + '_' + (now.getMonth() + 1) + '_' + now.getDate() + '_' + now.getHours() + '_' + now.getMinutes() + '_' + now.getSeconds() + '.xml'
          if ('download' in document.createElement('a')) {
            const elink = document.createElement('a')
            elink.download = fileName
            elink.style.display = 'none'
            elink.href = URL.createObjectURL(blob)
            document.body.appendChild(elink)
            elink.click()
            document.body.removeChild(elink)
            URL.revokeObjectURL(elink.href)
          } else {
            navigator.msSaveBlob(blob, fileName)
          }
          this.$message.success('在线转换成功')
        }).catch(() => {
          this.loading = false
          this.$message.error('在线转换失败')
        })
      })
    },
    submit() {
      this.$refs.upload.submit()
    }
  }
})

package main

import (
	"encoding/xml"
	"errors"
	"fmt"
	"io"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"time"

	xls "github.com/extrame/xls"
	"github.com/tealeg/xlsx"
)

const (
	Header       = `<?xml version="1.0" encoding="utf-8"?>` + "\n"
	XmlNameSpace = `http://ZTE.SRM.Supplier.Schemas.Report`
)

type Reserved struct {
	Name  string `xml:",attr"`
	Value string
}

type Serial struct {
	SerialNo string
	Reserved []Reserved
}

type Item struct {
	ItemNo          string
	ItemStyle       string
	BatchNo         string
	ManufactureDate string
	Serials         []Serial `xml:"Serials>Serial"`
}

type QualityReport struct {
	Xmlns         string `xml:"xmlns,attr"`
	TransactionID string
	SupplierNo    string
	SupplierName  string
	BrandName     string
	TotalQuantity int
	Date          string
	Time          string
	Items         []Item `xml:"Items>Item"`
}

func saveFile(formFile io.Reader, fileName string) error {
	inputExcelFile, err := os.Create("./upload/" + fileName)
	if err != nil {
		log.Printf("Create input excel file failed: %v\n", err)
		return err
	}
	defer inputExcelFile.Close()

	_, err = io.Copy(inputExcelFile, formFile)
	if err != nil {
		log.Printf("Write input excel file failed: %v\n", err)
		return err
	}

	return nil
}

func convertXLS2XML(r *http.Request, formFile multipart.File, header *multipart.FileHeader) (string, error) {
	transID := r.FormValue("TransactionID")
	supplierNo := r.FormValue("SupplierNo")
	supplierName := r.FormValue("SupplierName")
	brandName := r.FormValue("BrandName")
	itemNo := r.FormValue("ItemNo")
	itemStyle := r.FormValue("ItemStyle")
	batchNo := r.FormValue("BatchNo")
	manufactureDate := r.FormValue("ManufactureDate")

	item := Item{
		ItemNo:          itemNo,
		ItemStyle:       itemStyle,
		BatchNo:         batchNo,
		ManufactureDate: manufactureDate,
	}

	xlFile, err := xls.OpenReader(formFile, "utf-8")
	if err != nil {
		//try xlsx
		xlsxFile, err := xlsx.OpenReaderAt(formFile, header.Size)
		if err != nil {
			log.Printf("Can not open xls file %s: %v", header.Filename, err)
			return "", err
		}
		if len(xlsxFile.Sheets) == 0 {
			log.Printf("Can not open sheet in excel file %s", header.Filename)
			return "", errors.New("can not open sheet in excel file")
		}
		sheet := xlsxFile.Sheets[0]

		rowsCount := len(sheet.Rows)
		if rowsCount < 2 {
			log.Printf("Not enough rows in excel file %s", header.Filename)
			return "", errors.New("not enough rows in excel file")
		}

		var names []string
		for _, cell := range sheet.Rows[0].Cells {
			names = append(names, cell.String())
		}
		//Remove first col, since it is the serialNo
		names = names[1:]

		for i := 1; i < rowsCount; i++ {
			row := sheet.Rows[i]

			serial := Serial{
				SerialNo: row.Cells[0].String(),
			}

			for j, name := range names {
				if row.Cells[j+1].String() != "" {
					reserved := Reserved{
						Name:  name,
						Value: row.Cells[j+1].String(),
					}
					serial.Reserved = append(serial.Reserved, reserved)
				}
			}

			item.Serials = append(item.Serials, serial)
		}
	} else {
		//only get data from sheet 1
		sheet := xlFile.GetSheet(0)
		if sheet == nil {
			log.Printf("Can not open sheet in excel file %s", header.Filename)
			return "", errors.New("can not open sheet in excel file")
		}

		titles := sheet.Row(0)
		if titles == nil {
			log.Printf("Can not get keys in excel file %s", header.Filename)
			return "", errors.New("can not get keys in excel file")
		}

		var names []string
		//first col is SerialNo, so reserved name start from second col
		for i := 1; i <= titles.LastCol(); i++ {
			if titles.Col(i) != "" {
				names = append(names, titles.Col(i))
			}
		}

		for i := 1; i <= int(sheet.MaxRow); i++ {
			r := sheet.Row(i)

			serial := Serial{
				SerialNo: r.Col(0),
			}

			for j, name := range names {
				if r.Col(j+1) != "" {
					reserved := Reserved{
						Name:  name,
						Value: r.Col(j + 1),
					}
					serial.Reserved = append(serial.Reserved, reserved)
				}
			}

			item.Serials = append(item.Serials, serial)
		}
	}

	now := time.Now()
	report := QualityReport{
		Xmlns:         XmlNameSpace,
		TransactionID: transID,
		SupplierNo:    supplierNo,
		SupplierName:  supplierName,
		BrandName:     brandName,
		Date:          now.Format("20060102"),
		Time:          now.Format("030405") + fmt.Sprintf("%03d", now.Nanosecond()/1000000),
	}

	report.Items = append(report.Items, item)
	report.TotalQuantity = len(item.Serials)

	v, err := xml.MarshalIndent(report, "", "  ")
	if err != nil {
		log.Printf("marshal xml value error, error msg: %v", err)
		return "", err
	}
	return Header + string(v), nil
}

func xls2xml(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		// Parse our multipart form, 64 << 20 specifies a maximum
		// upload of 64 MB files.
		r.ParseMultipartForm(64 << 20)

		// FormFile returns the first file for the given key `uploadFile`
		// it also returns the FileHeader so we can get the Filename,
		// the Header and the size of the file
		formFile, handler, err := r.FormFile("file")
		if err != nil {
			log.Printf("Error Retrieving the File: %v\n", err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		defer formFile.Close()

		log.Printf("Uploaded File: %v, File Size: %v, MIME Header: %v\n",
			handler.Filename, handler.Size, handler.Header)

		/*
			if err = saveFile(formFile, handler.Filename); err != nil {
				return
			}
		*/

		resp, err := convertXLS2XML(r, formFile, handler)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		fmt.Fprintf(w, resp)
	} else {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
}

func main() {
	fs := http.FileServer(http.Dir("html"))
	http.Handle("/html/", http.StripPrefix("/html/", fs))
	http.HandleFunc("/xls2xml", xls2xml)
	http.ListenAndServe(":8080", nil)
}
